This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
## IMPORTANT! 
1. Firtsly run `yarn build`, because otherwise the app cannot read api urls from .env folder.
2. Finally, to start the app, run command `yarn start`.


## Available Scripts

In the project directory, you can run:

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.


## TESTING: 

Selenium test suit is in a file "Conferences_Service_FrontEnd_TestSuite.side", it can be opened with Selenium IDE https://chrome.google.com/webstore/detail/selenium-ide/mooikfkahbdckldjjndioackbalphokd.
It works, when backend is built and running, and no changes are made to database before runnibg tests.


##
