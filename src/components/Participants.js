
import React from 'react';
import {Link} from 'react-router-dom';
import {getAllParticipants} from '../controllers/ParticipantController';
import {parseDate} from '../utils/dateParser';

class Participants extends React.Component {
  _isMounted = false;
  constructor(props) {
    super(props);
    this.state = {
      participants: undefined
    };
    
  }
  componentDidMount = () =>{
    this._isMounted = true;
    getAllParticipants(this);
  }
  componentWillUnmount() {
      this._isMounted = false;
  }

  displayRowHeader(){
    return(
        <div key="-1"  className="row font-weight-bold border-bottom">
            <div className="col-sm">
                Full Name
            </div>
            <div className="col-sm">
                Birth Date
            </div>
        </div>
    );
}

  displayParticipants = () => {
    let participantsData = [];
    if(!this.state.participants){
        return participantsData;
    }
    let participantUrl = "/participants/";
    participantsData.push(
      this.displayRowHeader()
    );
    this.state.participants.forEach((p,i) => {
      participantsData.push(
          <div key={i}  className="row border-bottom">
            <div className="col-sm">
                <Link to={participantUrl+p.id} className="">{p.fullName}</Link>
            </div>
            <div className="col-sm">
                {parseDate(p.birthDate, false)}
            </div>
          </div>
        )
    })
    return participantsData;
}
render(){
  let participantsData = this.displayParticipants();
  return (
    <div className="justify-content-center">
      <h2>Participants</h2>
      <div className="justify-content-center px-50">{participantsData}</div>
    </div>
  );
}
}


export default Participants;
