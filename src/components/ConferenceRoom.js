import React from 'react';

function ConferenceRoom(props){
    let conference = props.conference;
    let conference_availability = props.conference_availability;
    let hasConfRoom = conference.conferenceRoom;
    let confRoom = [];
    let classStyle = props.isCancelled? "display:none" :conference_availability === 0? "border border-default rounded" : "border border-success rounded";
    
    if(hasConfRoom){
        confRoom =(
            <div key="cr">
                <div className="text-center font-weight-bold" >
                    <b>{conference.conferenceRoom.name}</b>
                </div>
                <div>
                    {conference.conferenceRoom.location}
                </div>
                <div className="font-italic">
                    {"capacity: " +  conference.conferenceRoom.maxSeats}
                </div>
                <div className={classStyle}> 
                    {!props.isCancelled && "# of available seats:"}
                    <h5 className="display-5">
                        {conference_availability}
                    </h5>
                </div>
            </div>
        );
    }
    return confRoom;
}
export default ConferenceRoom;