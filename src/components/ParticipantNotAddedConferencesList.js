import React from 'react';
import {addParticipantToConference} from '../controllers/ParticipantController';
import ConferenceName from '../ui_elements/ConferenceName';
import StartEndDisplayer from '../ui_elements/StartEndDisplayer';
import {arraySort} from '../utils/sorting';
import {clashingTimes} from '../utils/timeClashing';
import {notifications} from '../data/notifications';


class ParticipantNotAddedConferencesList extends React.Component{
    _isMounted = false;
    constructor(props) {
        super(props);
        this.state = {
            participantId:this.props.participantId,
            addResult : this.props.addResult,
            removeResult : this.props.removeResult
        };
    }

    componentDidMount=()=>{
        this._isMounted = true;
    }
    componentWillUnmount() {
        this._isMounted = false;
    }

    

    addToconference = (evt, confId) =>{
        if (evt) evt.preventDefault();
        if(this.state.participantId){
            let addToConfUrl = this.props.participantLinks.addToConference.href;
            addParticipantToConference(this, addToConfUrl, JSON.stringify(confId));
        }
    }

    computeAddableConferences = (confData, confAvailabilityList) =>{
        let allConferences = this.props.conferences;
        //no conferences at all
        if(!allConferences){
            return confData;
        }
        let participant_conferences = this.props.participant_conferences;

        //conferences, to where participant is not added
        let particNotAddedConferences = undefined;

        //participant is not added to any conference?
        let participantHasNoConferences = (!participant_conferences || participant_conferences.length === 0)? true : false ;
        
        if(participantHasNoConferences){
            particNotAddedConferences = allConferences;
        }
        else{
            let participantConfIds = participant_conferences.map(pc => {return pc.id});
            //filter out confs where participant have been added to already and where is not seats left
            let filtered = confAvailabilityList ? allConferences.filter(c => !participantConfIds.includes(c.id)) : allConferences;
                
            // add time clash warning to the be-added-conference
            particNotAddedConferences = clashingTimes(participant_conferences, filtered);
        }

        //filter out conferences in the past
        //  and where are not seats left
        let futureConferences = [];
        for(let c of particNotAddedConferences){
            if(c.startDate >= new Date().getTime() && (confAvailabilityList[c.id] != 0)){
                futureConferences.push(c);
            }
        }
        
        
        if(!futureConferences){
            return confData;
        }

        return arraySort(futureConferences, "startDate");
    }

    displayParticipantNotAddedConferences = () => {
        let confData = [];
        let confAvailabilityList = this.props.confAvailabilityList;
        let particNotAddedConferences_sorted = this.computeAddableConferences(confData, confAvailabilityList);
        particNotAddedConferences_sorted.forEach((c, i) => {
            if(!c.isCancelled)
            confData.push(
                <div key={i}  className="row border-bottom py-4">
                    <div className="col-sm">
                        <ConferenceName
                            name = {c.name}
                            isCancelled = {c.isCancelled}
                        />
                        {confAvailabilityList &&
                            <div className="border border-success rounded"> 
                                availability: 
                                <h5 className="display-5">
                                    {confAvailabilityList[c.id]}
                                </h5>
                            </div>
                        }
                    </div>
                    <div className="col-sm">
                        <StartEndDisplayer
                            startDate={c.startDate}
                            endDate={c.endDate}
                        />
                        {c.hasWarning &&
                            <h5 className="display-5 text-warning tilted">
                                {notifications.time_clash}
                            </h5>
                        }
                    </div>
                    <div className="col-sm">
                        <div> Room:</div>
                        {c.conferenceRoom.name}
                    </div>
                    <div className="col-sm">
                        <button onClick={(e) => this.addToconference(e, c.id)}
                            className="submit btn btn-success">
                                Add participant to conference
                        </button>
                    </div>
                </div>
            );
        });
        return confData;
    }

    render(){
        let otherConfs = this.displayParticipantNotAddedConferences();
        return(
            <div>
                {otherConfs}
            </div>
        );
    }
}
export default ParticipantNotAddedConferencesList;