import React from 'react';

class Calendar extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            datetime:undefined
        }
    }

    render() {
        return (
            <div className="form-group row">
                <label  className="col-sm text-right w-50" htmlFor={this.props.id}>
                    {this.props.label}
                </label>
                <input type="datetime-local" 
                    id={this.props.id}
                    className="form-control col-sm text-left w-50" 
                    value={this.state.datetime}
                    onChange={this.props.handleTimeChange}
                    required/>
            </div>
        );
    }

}

export default Calendar;