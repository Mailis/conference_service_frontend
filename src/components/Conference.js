import React from 'react';
import {getAllConferences} from '../controllers/ConferenceController';
import ConferenceForm from './ConferenceForm';
import ConferenceList from '../components/ConferenceList';
import {arraySort} from '../utils/sorting';

class Conference extends React.Component {
    _isMounted = false;
    constructor(props) {
        super(props);
        this.state = {
            conferences: undefined,
            confListLinks : undefined,
            rooms: undefined
        }
    }

    componentDidMount(){
        this._isMounted = true;
        getAllConferences(this);
    }

    componentWillUnmount() {
        this._isMounted = false;
    }
   
    displayCreatedConference = (conference, confroom) => {
        if(conference){
            let confList = this.state.conferences? this.state.conferences : [];
            conference.conferenceRoom = confroom;
            confList.push(conference);
            let conferences = arraySort(confList, "startDate");
            this.setState({conferences});
        }
    }

    render() {
        let conferenceData = this.state.conferences;
        return (
          <div className="justify-content-center">
            <h2>Conferences</h2>
            
            <h4 className="text-light bg-dark">CREATE CONFERENCE</h4>
            <ConferenceForm
                conferenceList = {conferenceData}
                updateConferenceList={this.displayCreatedConference}
                confstate = {this.state}
            />
            {conferenceData &&
                <div>
                    <h4 className="text-light bg-dark">CONFERENCES</h4>
                    <ConferenceList
                        conferenceList = {conferenceData}
                        confListLinks = {this.state.confListLinks}
                    />
                </div>
            }

          </div>
        );
      }
}

export default Conference;
