
import React from 'react';
import {getOneParticipant, getOneParticipantConferences} from '../controllers/ParticipantController';
import {getAllConferences, getConferenceMultiAvailability} from '../controllers/ConferenceController';
import ParticipantConferencesList from '../components/ParticipantConferencesList';
import ParticipantNotAddedConferencesList from '../components/ParticipantNotAddedConferencesList';
import {parseDate} from '../utils/dateParser';
import '../style/design.css';

class Participant extends React.Component {
    _isMounted = false;
    constructor(props) {
        super(props);
        this.state = {
            participantId:this.props.match.params.id,
            participant: undefined,
            participant_conferences: [],
            conferences: [],
            confListLinks: undefined,
            removeResult : undefined,
            addResult : undefined,
            confAvailab: undefined
        };
    }

    componentDidMount=()=>{
        this._isMounted = true;
        getOneParticipant(this, this.state.participantId);
        getOneParticipantConferences(this, this.state.participantId);
        getAllConferences(this);
        this.setConferenceAvailabilities();
    }

    componentDidUpdate(){
        if(!this.state.confAvailab){
            this.setConferenceAvailabilities();
        }
    }

    componentWillUnmount() {
        this._isMounted = false;
    }
    
    setConferenceAvailabilities=()=>{
        if(this.state.confListLinks){
            let url = this.state.confListLinks.all_availabilities.href;
            getConferenceMultiAvailability(this, url);
        }
    }

    handleRemoveParticipantFromConference = (removeResult) =>{
        this.setState({removeResult})
        if(removeResult){
            let confAvailabilityList = this.state.confAvailab;
            let participant_conferences_tmp = this.state.participant_conferences;
            let removedConfID = removeResult.conferenceId;
            if(confAvailabilityList){
                confAvailabilityList[removedConfID] = confAvailabilityList[removedConfID] + 1;
            }
            let participant_conferences = participant_conferences_tmp.filter(c => c.id != removedConfID);
            this.setState({removeResult, participant_conferences, confAvailab : confAvailabilityList});
        }
    }

    handleAddParticipantToConference = (addResult) =>{
        if(addResult){
            let participant_conferences = this.state.participant_conferences;
            let all_conferences = this.state.conferences;
            let addedConfID = addResult.conferenceId;
            let addedConference = all_conferences.filter(c => c.id == addedConfID);
            if(addedConference && addedConference.length > 0){
                if(!participant_conferences.includes(addedConference[0])){
                    participant_conferences.push(addedConference[0]);
                }
            }
            let confAvailabilityList = this.state.confAvailab;
            if(confAvailabilityList){
                confAvailabilityList[addedConfID] = confAvailabilityList[addedConfID] - 1;
            }
            this.setState({addResult, participant_conferences, confAvailab : confAvailabilityList})
        }
    }    

    displayParticipant= () => {
        let participantData = [];
        if(!this.state.confListLinks){
            return participantData;
        }

        let participant = this.state.participant;
        if(!participant){
            return participantData;
        }

        participantData.push(
            <div className="justify-content-center" key="-1">
                <h4 className="text-light bg-dark">
                    {participant.fullName}
                </h4>
                <div className="">
                    <h4 className=""> Birth date: </h4>
                    {parseDate(participant.birthDate, false)}
                </div>
                <div className="row col w-80 ">
                    <div className="col px-15">
                        <h6 className=""> Participating Conferences: </h6>
                        <ParticipantConferencesList
                            participantId = {this.state.participantId}
                            participantLinks = {this.state.participant._links}
                            participant_conferences = {this.state.participant_conferences}
                            conferences = {this.state.conferences}
                            handleRemoveParticipant = {this.handleRemoveParticipantFromConference}
                            addResult = {this.state.addResult}
                            removeResult = {this.state.removeResult}
                        />
                    </div>

                    <div className="col px-15">
                        <h6 className=""> Add to Conference: </h6>
                        <div>
                            <ParticipantNotAddedConferencesList
                                participantId = {this.state.participantId}
                                participant_conferences = {this.state.participant_conferences}
                                participantLinks = {this.state.participant._links}
                                conferences = {this.state.conferences}
                                confAvailabilityList = {this.state.confAvailab}
                                confListLinks = {this.state.confListLinks}
                                handleAddParticipant = {this.handleAddParticipantToConference}
                                addResult = {this.state.addResult}
                                removeResult = {this.state.removeResult}
                            />
                        </div>
                    </div>
                </div>
            </div>
        );
        return participantData;
    }

    render(){
        let participant = [];
        if(this.state.confListLinks && this.state.confAvailab)
            participant = this.displayParticipant();
        return (
            <div className="justify-content-center">
                <h2>Participant</h2>
                <div className="justify-content-center px-50">{participant}</div>
            </div>
        );
    }
}


export default Participant;
