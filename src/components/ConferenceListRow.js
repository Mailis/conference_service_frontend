import React from 'react';
import StartEndDisplayer from '../ui_elements/StartEndDisplayer';
import Toggler from '../components/Toggler';
import ConferenceRoom from '../components/ConferenceRoom';
import ConferenceName from '../ui_elements/ConferenceName';

function ConferenceListRow(props){
    const confLinks = props.conference._links;
    const index = props.index;
    const participants = props.participants;
    const c = props.conference;
    const isCancelledStyle = c.isCancelled? " text-black-50 "  : ""; 

    let conferenceDataRow = [];
    
    conferenceDataRow.push(
        <div key={index}  className={"row border-bottom py-4 " + isCancelledStyle}>
            <div className="col-sm">
                <ConferenceName
                    name = {c.name}
                    isCancelled = {c.isCancelled}
                />
            </div>
            <div className="col-sm">
                <StartEndDisplayer
                    startDate={c.startDate}
                    endDate={c.endDate}
                />
            </div>
            <div className="col-sm">
                <ConferenceRoom
                    isCancelled = {c.isCancelled}
                    conference = {c}
                    conference_availability = {props.conference_availability}
                />
            </div>
            <div className="col-sm">
                <Toggler 
                    participants = {participants}
                />
            </div>
            <div className="col-sm">
                <button type="button" 
                        className="btn btn-danger"
                        onClick={(e) => props.handleDeleteConference(e, confLinks.delete.href, c.id)}>
                    Delete
                </button>
                <br />
                {!c.isCancelled &&
                    <button type="button" 
                            className="btn btn-warning"
                            onClick={(e) => props.handleCancelConference(e, confLinks.update.href, c.id)}>
                        Cancel
                    </button>
                }
            </div>
        </div>
    );
    return conferenceDataRow;
}

export default ConferenceListRow;