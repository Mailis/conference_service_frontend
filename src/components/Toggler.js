import React from 'react';
import caret_right from '../icons/caret_right.svg';
import caret_down from '../icons/caret_down.svg';

class Toggler extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            togglerOn: false
        }
    }

    
    toggle = (evt) => {
        if(evt){
            evt.preventDefault();
        }
        this.setState({togglerOn : !this.state.togglerOn});
    }

    displayToggler(){
        let listVisibility = this.state.togglerOn? {visibility:'visible'} : {display:'none'};
        return(
            <div>
                {this.props.participants.length}
                <button className="px-10 btn "onClick={this.toggle}>
                    <img src={this.state.togglerOn? caret_down : caret_right} 
                        alt={this.state.togglerOn? "opened" : "closed"} />
                </button>
                <ol  className="row" style={listVisibility}>
                    {this.props.participants}
                </ol>
            </div>
        );
    }

    render() {
        let toggler = this.displayToggler();
        return (
            <div>{toggler}</div>
            
        );
    }
}

export default Toggler;