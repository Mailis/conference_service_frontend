import React from 'react';
import {removeParticipantFromConference} from '../controllers/ParticipantController';
import ConferenceName from '../ui_elements/ConferenceName';
import StartEndDisplayer from '../ui_elements/StartEndDisplayer';
import {arraySort} from '../utils/sorting';

class ParticipantConferencesList extends React.Component{
    _isMounted = false;
    constructor(props) {
        super(props);
        this.state = {
            addResult : this.props.addResult,
            removeResult : this.props.removeResult
        };
    }

    componentDidMount=()=>{
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    removeFromconference = (evt, confId) =>{
        if (evt) evt.preventDefault();
        let removeFromConfUrl = this.props.participantLinks.removeFromConference.href;
        removeParticipantFromConference(this, removeFromConfUrl, JSON.stringify(confId));
    }

    displayParticipantConferences = () => {
        let participantConfData = [];

        let particConfs = this.props.participant_conferences;
        if(!particConfs){
            return participantConfData;
        }

        let pConfs = arraySort(particConfs, "startDate");
        pConfs.forEach((c, i) => {
            const isCancelledStyle = c.isCancelled? " text-black-50 "  : ""; 
            //let links = c._links;
            participantConfData.push(
                <div key={i}  className={"row border-bottom py-4 " + isCancelledStyle}>
                    <div className="col-sm">
                        <ConferenceName
                            name = {c.name}
                            isCancelled = {c.isCancelled}
                        />
                    </div>
                    <div className="col-sm">
                        <StartEndDisplayer
                            startDate={c.startDate}
                            endDate={c.endDate}
                        />
                    </div>
                    <div className="col-sm">
                        <div> Room:</div>
                        {c.conferenceRoom.name}
                    </div>
                    <div className="col-sm">
                        <button onClick={(e) => this.removeFromconference(e, c.id)}
                            className="submit btn btn-danger">
                                Remove participant from conference
                        </button>
                    </div>
                </div>
            );
        });
        return participantConfData;
    }
    render(){
        let participant_confs = this.displayParticipantConferences();
        return (
            <div>
                {participant_confs}
            </div>
        );
    }
}

export default ParticipantConferencesList;