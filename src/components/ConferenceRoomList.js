
import React from 'react';

import {getAllConferenceRooms} from '../controllers/ConfRoomController';

class ConferenceRoom extends React.Component {
  _isMounted = false;
  constructor(props) {
    super(props);
    this.state = {
        rooms: undefined
    };
    
  }
  componentDidMount(){
    this._isMounted = true;
    getAllConferenceRooms(this);
  }
  
  componentWillUnmount() {
      this._isMounted = false;
  }

  displayRowHeader(){
    return(
        <div key="-1"  className="row font-weight-bold border-bottom">
            <div className="col-sm">
                Name
            </div>
            <div className="col-sm">
                Location
            </div>
            <div className="col-sm">
                Capacity
            </div>
        </div>
    );
}

  displayRooms = () => {
    let roomsData = [];
    if(!this.state.rooms){
        return roomsData;
    }
    roomsData.push(
      this.displayRowHeader()
    );
    this.state.rooms.forEach((r,i) => {
        roomsData.push(
          <div key={i}  className="row border-bottom">
            <div className="col-sm">
                {r.name}
            </div>
            <div className="col-sm">
                {r.location}
            </div>
            <div className="col-sm">
                {r.maxSeats}
            </div>
          </div>
        )
    })
    return roomsData;
}
render(){
  let roomsData = this.displayRooms();
  return (
    <div className="justify-content-center">
      <h2>Conference Rooms</h2>
      <div className="justify-content-center px-50">{roomsData}</div>
    </div>
  );
}
}

export default ConferenceRoom;
