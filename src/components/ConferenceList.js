import React from 'react';
import ConferenceListRow from '../components/ConferenceListRow';
import {deleteConference, updateConference, getConferenceMultiAvailability} from '../controllers/ConferenceController';
import {arraySort} from '../utils/sorting';
import {parseDate} from '../utils/dateParser';

class ConferenceList extends React.Component  {
    _isMounted = false;
    constructor(props) {
        super(props);
        this.state = {
            conferences: props.conferenceList,
            confListLinks : props.confListLinks,
            confAvailab: {}
        }
    }

    componentDidMount(){
        this._isMounted = true;
        this.setConferenceAvailabilities();
    }
    
    componentWillUnmount() {
        this._isMounted = false;
    }

    setConferenceAvailabilities = () => {
        let url = this.state.confListLinks.all_availabilities.href;
        getConferenceMultiAvailability(this, url);
    }

    removeConference = (evt, deleteUrl, id) => {
        if(evt){
            evt.preventDefault();
        }
        let stateConferences = this.state.conferences;
        let filteredConferences = stateConferences.filter(c => c.id !== id);
        deleteConference(this, deleteUrl, filteredConferences);
    }

    cancelConference = (evt, updateUrl, id) => {
        if(evt){
            evt.preventDefault();
        }
        let stateConferences = this.state.conferences;
        let filteredConferences = stateConferences.filter(c => c.id === id);
        let confToUpdate = undefined;
        if(filteredConferences.length>0){
            confToUpdate = filteredConferences[0];
            confToUpdate.isCancelled = true;
        }
        for(let c of stateConferences){
            if(c.id === id) 
                c.isCancelled=true;
        }
        if(confToUpdate)
            updateConference(this, updateUrl, JSON.stringify(confToUpdate), stateConferences);
    }
    
    displayConferenceParticipants(conference){
        let participants = [];
        //sort participants by fullName
        let parts = arraySort(conference.participants, "fullName");
        parts.forEach((p,pi) => {
            participants.push(
                    <li key={"P"+pi}>
                        <div className="col-sm">
                            {p.fullName}
                        </div>
                        <div className="col-sm">
                            {parseDate(p.birthDate)}
                        </div>
                    </li>
            )
        })
        return participants;
    }
    
    displayConferenceRowHeader(){
        return(
            <div key="-1"  className="row font-weight-bold border-bottom">
                <div className="col-sm">
                    Conference Name
                </div>
                <div className="col-sm">
                    Conference Date
                </div>
                <div className="col-sm">
                    Room
                </div>
                <div className="col-sm">
                    Participants
                </div>
                <div className="col-sm">
                    Cancel/Remove Conference 
                </div>
            </div>
        );
    }

    displayAllConferences(){
        if(!this.state.conferences){
            return [];
        }
        let conferenceData = [];
        // Table headers
        conferenceData.push(
            this.displayConferenceRowHeader()
        );
        // conference rows
        this.state.conferences.forEach((c, i) => {
            let participants = this.displayConferenceParticipants(c);
            conferenceData.push(
                <ConferenceListRow key={"ConferenceListRow_"+i}
                    conference = {c}
                    index = {i}
                    participants = {participants}
                    handleDeleteConference = {this.removeConference}
                    handleCancelConference = {this.cancelConference}
                    conference_availability = {this.state.confAvailab[c.id]}
                />
            );
        });
        return conferenceData;
    }

    render(){
        const confList = this.displayAllConferences();
        
        return (
            <div>
                {confList}
            </div>
        );
    }
}  
export default ConferenceList;