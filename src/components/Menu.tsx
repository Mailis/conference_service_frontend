import React from 'react';
import {Link} from 'react-router-dom';
import {menu_data} from '../data/menuitems';

function Menu() {

    function getMenu(){
        let menu:any = [];
        menu_data.forEach((m, index) => {
            const url:string|undefined= m.url;
            const title:string|undefined= m.title;
            menu.push(
                    <li key = {index}  className="nav-item active">
                        <Link to={url}  className="nav-link">{title}</Link>
                    </li>
            );
        });
        return menu;
    }

    const menu = getMenu();
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light ">
        <ul className="navbar-nav mr-auto">
          {menu}
        </ul>
    </nav>
  );
}

export default Menu;
