import React from 'react';
import {getAllConferenceRooms} from '../controllers/ConfRoomController';
import {modifyResource} from '../controllers/crudController';
import {hasTimeClash, isLogicalTime, isInFutureTime} from '../utils/timeClashing';
import Calendar from './Calendar';
import ErrorMessage from '../ui_elements/ErrorMessage';


class ConferenceForm extends React.Component {
    _isMounted = false;
    constructor(props) {
        super(props);
        this.state = {
            confname: "",
            selectedRoomId: "",
            startDateTime: "",
            endDateTime: "",
            hasTimeClashing:false,
            isStartTimeInFuture:true,
            isEndTimeInFuture:true,
            isLogicalDateTime:true,
            rooms: undefined
        };
        this.timeClashErrorMessage = "SAME ROOM EVENTS MUST NOT OVERLAP TEMPORALLY.";
        this.timeStartOrEndInPastErrorMessage = "EVENT'S DATES MUST BE IN FUTURE.";
        this.timeStartIsAfterEndErrorMessage = "EVENT'S START MUST BE BEFORE END.";
        this.timeStartEndAreEualErrorMessage = "EVENT'S START AND END MUST NOT BE EQUAL.";
    }
    componentDidMount = () => {
        this._isMounted = true;
        getAllConferenceRooms(this);
    }
    
    componentWillUnmount() {
        this._isMounted = false;
    }
    
    createConference = evt => {
        if(evt){
            evt.preventDefault();
        }
        //data must be set and with no time clash
        if(this.state.selectedRoomId && this.state.confname &&
                this.state.startDateTime && this.state.endDateTime &&
                this.state.isStartTimeInFuture && this.state.isEndTimeInFuture && 
                this.state.isLogicalDateTime && !this.state.hasTimeClashing){

            this.createNewConfereceObject();
        }
    }

    createNewConfereceObject = () =>{
        let newConference = {
            name:this.state.confname,
            startDate:Date.parse(this.state.startDateTime),
            endDate:Date.parse(this.state.endDateTime)
        }
        //conference room
        let selectedRoomId = Number(this.state.selectedRoomId);
        let confRoom = undefined;
        if(selectedRoomId){
            newConference.conferenceRoom = {id:selectedRoomId};
            let roomsList = this.state.rooms;
            if(roomsList)
                confRoom = roomsList.filter(r => r.id === selectedRoomId)[0];
        }
        const confUrl = process.env.REACT_APP_CONFERENCE_API_URL;
        let response = modifyResource(confUrl, "POST", JSON.stringify(newConference));

        response.then((conference)=>{
            this.props.updateConferenceList(conference, confRoom);
        });
    }


    validateRoomAvailability = (startDateTime, endDateTime, conferenceRoomId) => {
        startDateTime = new Date(startDateTime).getTime();
        endDateTime = new Date(endDateTime).getTime();
        //don't consider cancelled conferences for time-room clash check
        let existingConferences = this.props.conferenceList.filter(c => !c.isCancelled);
        let hasDateTimeClashing = false;
        if(existingConferences){
            for(let ec of existingConferences){
                let existingID = "" + ec.conferenceRoom.id;
                if(existingID === conferenceRoomId){
                    //validate against 2 hours difference
                    if(hasTimeClash(ec.startDate, ec.endDate, startDateTime, endDateTime)){
                        hasDateTimeClashing = true;
                        break;
                    }
                }
            }
        }
        return hasDateTimeClashing;
    }

    handleNameChange = event => this.setState({confname:event.target.value});
    
    onStartDateTimeChange = (evt) => {
        if(evt){
            evt.preventDefault();
        }
        let startDateTime = evt.target.value;
        let isStartTimeInFuture = isInFutureTime(new Date(startDateTime).getTime());
        let isLogicalDateTime = this.dateTimeChangeLogicsControl(startDateTime, this.state.endDateTime);
        let hasTimeClashing = this.dateTimeChangeClashControl(startDateTime, this.state.endDateTime);
        this.setState({ 
            startDateTime,
            hasTimeClashing,
            isStartTimeInFuture,
            isLogicalDateTime
        });
    }

    onEndDateTimeChange = (evt) => {
        if(evt){
            evt.preventDefault();
        }
        let endDateTime = evt.target.value;
        let isEndTimeInFuture = isInFutureTime(new Date(endDateTime).getTime());
        let isLogicalDateTime = this.dateTimeChangeLogicsControl(this.state.startDateTime, endDateTime);
        let hasTimeClashing = this.dateTimeChangeClashControl(this.state.startDateTime, endDateTime);
        this.setState({ 
            endDateTime,
            hasTimeClashing,
            isEndTimeInFuture,
            isLogicalDateTime
        });
    }

    dateTimeChangeLogicsControl = (startDateTime, endDateTime) =>{
        let isLogicalDateTime = this.state.isLogicalDateTime;
        if(startDateTime && endDateTime){
            isLogicalDateTime = isLogicalTime(new Date(startDateTime).getTime(), new Date(endDateTime).getTime());
        }
        return isLogicalDateTime;
    }

    dateTimeChangeClashControl = (startDateTime, endDateTime) =>{
        let hasTimeClashing = this.state.hasTimeClashing;
        if(this.state.selectedRoomId && startDateTime && endDateTime){
            hasTimeClashing = this.validateRoomAvailability(startDateTime, endDateTime, this.state.selectedRoomId);
        }
        return hasTimeClashing;
    }

    handleSelectRoomChange = evt => {
        if(evt){
            evt.preventDefault();
        }
        let roomId = evt.target.value;
        let hasDateTimeClashing = false;
        if(this.state.startDateTime && this.state.endDateTime){
            hasDateTimeClashing = this.validateRoomAvailability(this.state.startDateTime, this.state.endDateTime, roomId);
        }
        this.setState({ 
            selectedRoomId : roomId,
            hasTimeClashing:hasDateTimeClashing
        });
    }

    buildRoomOptions = () => {
        let roomsData = [];
        if(!this.state.rooms){
            return roomsData;
        }
        roomsData.push(
            <option key="options">Select a Room </option>
        )
        this.state.rooms.forEach((r,i) => {
            roomsData.push(
                <option key={"option_" + r.id}
                        value = {r.id}>
                    {r.name}
                </option>
            )
        })
        return roomsData;
    }

    getErrorMessages = () =>{
        let urror = [];
        if(!this.state.isStartTimeInFuture || !this.state.isEndTimeInFuture){
            urror.push(
                <ErrorMessage key={"err0"}
                    message = {this.timeStartOrEndInPastErrorMessage} 
                />
            );
        }
        if(!this.state.isLogicalDateTime){
            urror.push(
                <ErrorMessage key={"err1"}
                    message = {this.timeStartIsAfterEndErrorMessage} 
                />
            );
        }
        if(this.state.startDateTime && this.state.endDateTime && 
            this.state.startDateTime === this.state.endDateTime){
            urror.push(
                <ErrorMessage key={"err2"}
                    message = {this.timeStartEndAreEualErrorMessage} 
                />
            );
        }
        if(this.state.hasTimeClashing){
            urror.push(
                <ErrorMessage  key={"err3"}
                    message = {this.timeClashErrorMessage} 
                />
            );
        }
        return urror;
    }

    render(){
        const roomOptions = this.buildRoomOptions();
        const buttonIsActive = (this.state.selectedRoomId && this.state.confname &&
                                this.state.startDateTime && this.state.endDateTime);
        const errorMessages = this.getErrorMessages();
        const hasError = errorMessages.length > 0;
        
        const submitBtnOrErrors = hasError? errorMessages : buttonIsActive?
            <button type="submit" className="btn btn-primary"> Add Conference </button>
            :
            <button type="submit" className="btn btn-primary" disabled> Add Conference </button>;
        

        return (
            <div className="row justify-content-center ">
                <div className="w-50 p-3">
                    <form  onSubmit={(e) => this.createConference(e)}>
                        <div className="form-group row">
                            <label  className="col-sm text-right w-50" htmlFor="conference_name">Conference name:</label>
                            <input type="text"
                                className="form-control col-sm text-left w-50" 
                                placeholder="Enter conference name"
                                value={this.state.confname} 
                                onChange={this.handleNameChange}
                                required
                            />
                        </div>
                        <Calendar 
                            label = {"Start Time:"}
                            handleTimeChange = {this.onStartDateTimeChange} 
                            id={"conference_start_time"}
                        />
                        <Calendar 
                            label = {"End Time:"}
                            handleTimeChange = {this.onEndDateTimeChange} 
                            id={"conference_end_time"}
                        />
                        <div className="form-group  row">
                            <label  className="col-sm text-right w-50" htmlFor="select_conference_room">Select a room:</label>
                            <select className="form-control selectpicker  text-left w-50"
                                id="select_conference_room"
                                value={this.state.selectedRoomId}
                                onChange={this.handleSelectRoomChange}
                                required>
                                {roomOptions}
                            </select>
                        </div>
                        
                        {submitBtnOrErrors}
                    </form>
                </div>
            </div>
        );
    }
}  
export default ConferenceForm;