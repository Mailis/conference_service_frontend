export function clashingTimes(existingConfArr, futureConfArr){
    if(!existingConfArr || existingConfArr.length === 0)
        return futureConfArr;
    let existingDates = existingConfArr.map(c => {return [c.startDate, c.endDate]});
    let modifiedFuture = [];
    for (let c of futureConfArr) {
        let hasWarning = false;
        for (const ed of existingDates) {
            if(hasTimeClash(ed[0], ed[1], c.startDate, c.endDate)){
                hasWarning = true;
                break;
            }
        }
        c.hasWarning = hasWarning;
        modifiedFuture.push(c);
    };
    return modifiedFuture;
}

export function hasTimeClash(dateTime1_start, dateTime1_end, dateTime2_start, dateTime2_end){
    let isClashing = false;
    if( (dateTime2_start >= dateTime1_start && dateTime2_start <= dateTime1_end) ||
        (dateTime2_end >= dateTime1_start && dateTime2_end <= dateTime1_end)  ||
        (dateTime1_start >= dateTime2_start && dateTime1_start <= dateTime2_end) ||
        (dateTime1_end >= dateTime2_start && dateTime1_end <= dateTime2_end) ){
        isClashing = true;
    }
    return isClashing;
}


export function isLogicalTime(dateTime_start, dateTime_end){
    return dateTime_end >= dateTime_start;
}

export function isInFutureTime(dateTime){
    return dateTime >= new Date().getTime();
}
