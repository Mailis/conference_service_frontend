export function parseDate( unix_timestamp, includeTime=true){
    const date = unix_timestamp.toString().length === 10 ? new Date(unix_timestamp*1000) : new Date(unix_timestamp);
    const year = date.getFullYear();
    const month = addZero(date.getMonth() + 1);
    const day = addZero(date.getDate());

    let formattedTime = day + "." + month + "." + year;

    if(includeTime){
        // Hours part from the timestamp
        const hours = date.getHours();
        // Minutes part from the timestamp
        const minutes = "0" + date.getMinutes();
        // Seconds part from the timestamp
            const seconds = "0" + date.getSeconds();
        // Will display time in 10:30:23 format
        formattedTime+= "  " + hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
    }

    return formattedTime;
}

function addZero(number){
    return number < 10? "0"+number : number;
}