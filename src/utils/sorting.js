export function arraySort(arr, property){
    arr.sort((a, b) => {
        if (a[property] < b[property]) 
            return -1
        return a[property] > b[property] ? 1 : 0
      })
      return arr;
}