import React from 'react';
import {Switch, Route, Router} from 'react-router-dom';
import { createBrowserHistory } from 'history';
import Menu from './components/Menu';
import Conference from './components/Conference';
import ConferenceRoomList from './components/ConferenceRoomList';
import Participants from './components/Participants';
import Participant from './components/Participant';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  const history = createBrowserHistory();
  return (
    <div className="App mb-5">
      <Router  history={history}>
        <header className="navbar-lg">
          <Menu />
        </header>
          <Switch>
            <Route exact path="/" component={Conference}/>
            <Route exact path="/conferences" component={Conference}/>
            <Route exact path="/conference_rooms" component={ConferenceRoomList}/>
            <Route exact path="/participants" component={Participants}/>}/>
            <Route exact path="/participants/:id" component={Participant}/>}/>
          </Switch>
      </Router>
    </div>
  );
}

export default App;
