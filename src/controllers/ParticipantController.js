
import {handleResource, modifyResource} from '../controllers/crudController';

export function getAllParticipants(that){
    const confParticipantUrl = process.env.REACT_APP_CONFERENCE_PARTICIPANT_API_URL;
    let data =  handleResource(confParticipantUrl, "GET");
    data.then(function(result) {
        if (that._isMounted) {
            let hasParticipants = result._embedded? true : false;
            if(hasParticipants){
                that.setState({participants : result._embedded.participantList});
            }
        }
    });
}


export function getOneParticipant(that, participantId){
    const participantUrl = process.env.REACT_APP_CONFERENCE_PARTICIPANT_API_URL + "/" + participantId;
    let data =  handleResource(participantUrl, "GET");
    data.then(function(result) {
        if (that._isMounted) {
            that.setState({participant : result});
        }
    });
}

export function getOneParticipantConferences(that, participantId){
    const participantUrl = process.env.REACT_APP_CONFERENCE_PARTICIPANT_API_URL + "/" + participantId + "/conference";
    let data =  handleResource(participantUrl, "GET");
    data.then(function(result) {
        if (that._isMounted) {
            if (result) {
                let hasConfList = result._embedded? true : false;
                if(hasConfList){
                    that.setState({participant_conferences : result._embedded.conferenceList});
                }
            }
        }
    });
}

export function addParticipantToConference(that, url, confId){
    let data =  modifyResource(url, "POST", confId);
    data.then(function(result) {
        if(that._isMounted) {
            if(result){
                that.setState({
                    addResult : result
                });
                that.props.handleAddParticipant(result);
            }
        }
    });
}

export function removeParticipantFromConference(that, url, confId){
    let data =  modifyResource(url, "DELETE", confId);
    data.then(function(result) {
        if (that._isMounted) {
            if(result){
                that.setState({
                    removeResult : result
                });
                that.props.handleRemoveParticipant(result);
            }
        }
    });
}
