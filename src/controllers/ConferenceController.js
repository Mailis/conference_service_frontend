import {handleResource, modifyResource} from '../controllers/crudController';

export function getAllConferences(that) {
    const confUrl = process.env.REACT_APP_CONFERENCE_API_URL;
    let data =  handleResource(confUrl, "GET");
    data.then(function(result) {
        if (that._isMounted) {
            let hasConferences = result._embedded? true : false;
            if(hasConferences){
                that.setState({
                    conferences : result._embedded.conferenceList,
                    confListLinks: result._links
                });
            }
        }
    });
}

export function deleteConference(that, deleteUrl, filteredConferences) {
    let data =  handleResource(deleteUrl, "DELETE");
    data.then(function(result) { 
        if (that._isMounted) {
            if(result)
                that.setState({conferences : filteredConferences});
        }
    });
}

export function updateConference(that, updateUrl, payload, filteredConferences){
    let data =  modifyResource(updateUrl, "PUT", payload);
    data.then(function(result) { 
        if (that._isMounted) {
            if(result)
                that.setState({conferences : filteredConferences});
        }
    });
}


export  function getConferenceMultiAvailability(that, availabilityUrl) {
    let data = handleResource(availabilityUrl, "GET");
    data.then(function(result){
        if (that._isMounted) {
            if(result)
                that.setState({confAvailab : result});
        }
    });
}
