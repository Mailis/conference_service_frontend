import {handleResource} from '../controllers/crudController';

export function getAllConferenceRooms(that){
    const confRoomUrl = process.env.REACT_APP_CONFERENCE_ROOM_API_URL;
    let data =  handleResource(confRoomUrl, "GET");
    data.then(function(result) {
        if (that._isMounted) {
            let hasRooms = result._embedded? true : false;
            if(hasRooms){
                that.setState({rooms : result._embedded.conferenceRoomList});
            }
        }
    });
}

