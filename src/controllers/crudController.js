export async function handleResource(url, method) {
    const headers = new Headers({
        'X-Custom-Header': 'ProcessThisImmediately',
        "Accept": "application/json, text/plain, */*"
    });

    let response = await fetch(
        url, 
        {
            method: method,
            headers: headers,
            mode: 'cors'
        }
    );
    
    if (response.ok) { 
        let json =  response.json();
        return json;
    } 
    else {
        console.error("Fetch handleResource " + method + " error: ", response.error);
    }
    
}
export async function modifyResource(url, method, payload) {
    const headers = new Headers({
        'Content-Type': 'application/json'
    });

    let response = await fetch(
        url, 
        {
            method: method,
            headers: headers,
            mode: 'cors',
            body: payload
        }
    );
    
    if (response.ok) { 
        let json =  response.json();
        return json;
    } 
    else {
        console.error("Fetch modifyResource: " + method + " error: ", response);
    }
    
}