import React from 'react';
import {notifications} from '../data/notifications';
import '../style/design.css';

function ConferenceName(props:any):any{
    return(
        <>
            <div> Name:</div>
                <b>{props.name}</b>
                {props.isCancelled &&
                    <>
                        <br />
                        <h5 className="display-5 tilted">{notifications.is_cancelled}</h5>
                    </>
                }
        </>
    );
}
export default ConferenceName;