import React from 'react';

function ErrorMessage(props:any):any{
    return(
        <div>
            <b className="text-danger">
                {props.message}    
            </b>
        </div>
    );
}

export default ErrorMessage;