import React from 'react';
import {parseDate} from '../utils/dateParser';

function StartEndDisplayer(props:any):any{
    return(
        <>
            <div> START: </div>
            <b>{parseDate(props.startDate)}</b>
            <div> END: </div>
            <span>{parseDate(props.endDate)}</span>
        </>
    );
}

export default StartEndDisplayer;